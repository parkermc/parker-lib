package com.parkermc.mod.proxy;

import java.io.File;

import com.parkermc.api.amnesia.AmnesiaEventHandler;
import com.parkermc.mod.AnnotatedUtil;
import com.parkermc.mod.ModConfig;
import com.parkermc.mod.ModMain;
import com.parkermc.mod.events.EventHandlerWorld;

import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.common.config.Configuration;
import net.minecraftforge.fml.common.event.FMLInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPostInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPreInitializationEvent;

public class ProxyCommon {
	public static Configuration config;
	private EventHandlerWorld eventHandlerWorldLoad = new EventHandlerWorld();
	
	public void preInit(FMLPreInitializationEvent event) {
		MinecraftForge.EVENT_BUS.register(eventHandlerWorldLoad);
		File directory = event.getModConfigurationDirectory();
        config = new Configuration(new File(directory.getPath(), ModMain.MODID+".cfg"));
        ModConfig.readConfig();
        
        AmnesiaEventHandler.register(AnnotatedUtil.getEvents(event.getAsmData()));
	}
	
	public void init(FMLInitializationEvent event) {
	}
	
	public void postInit(FMLPostInitializationEvent event) {
	}
}
