package com.parkermc.mod;

import com.parkermc.mod.proxy.ProxyCommon;

import net.minecraftforge.common.config.Configuration;
import net.minecraftforge.fml.client.event.ConfigChangedEvent;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;

public class ModConfig {
	private static final String CATEGORY_GENERAL = "general";
	public static String randomMsg = "[Amnesia] You have forgotten what you have once known";
	public static String normMsg = "[Amnesia] You have regained your memory";
	public static String postMsg = "";
	public static long expire = 6000; // five mins 
	
    public static void readConfig() {
        Configuration cfg = ProxyCommon.config;
        try {
            cfg.load();
            initGeneralConfig(cfg);
        } catch (Exception e1) {
            ModLog.error("Problem loading config file!", e1);
        } finally {
            if (cfg.hasChanged()) {
                cfg.save();
                cfg.load();
            }
        }
    }
    
    private static void initGeneralConfig(Configuration cfg) {
        cfg.addCustomCategoryComment(CATEGORY_GENERAL, "General configuration");
        randomMsg = cfg.getString("random_message", CATEGORY_GENERAL, randomMsg, "The message sent when the recipes have been randomised");
        normMsg = cfg.getString("normal_message", CATEGORY_GENERAL, normMsg, "The message sent when the recipes have been set back to normal");
        postMsg = cfg.getString("post_message", CATEGORY_GENERAL, postMsg, "The message sent after the recipes have been changed");
        expire = cfg.getInt("expire_time", CATEGORY_GENERAL, (int) expire, 0, 2147483647, "How long a recipe set lasts for in ticks 20=1sec, 1200=1min");
    }
    
    @Mod.EventBusSubscriber
	static class ConfigurationHandler {
		@SubscribeEvent
		public static void onConfigChanged(ConfigChangedEvent.OnConfigChangedEvent event) {
			if (event.getModID().equals(ModMain.MODID) && ProxyCommon.config.hasChanged()) {
				ProxyCommon.config.save();
				readConfig();
			}
		}
	}
}
