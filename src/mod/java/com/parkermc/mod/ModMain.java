package com.parkermc.mod;

import com.parkermc.api.API;
import com.parkermc.mod.proxy.ProxyCommon;

import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.Mod.EventHandler;
import net.minecraftforge.fml.common.SidedProxy;
import net.minecraftforge.fml.common.event.FMLInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPostInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPreInitializationEvent;

@Mod(modid = ModMain.MODID, name = ModMain.MODNAME, version = ModMain.VERSION, guiFactory = "com.parkermc.mod.gui.GuiModConfigFactory")
public class ModMain{
    public static final String MODID = "parkerlib";
    public static final String MODNAME = "Parker Lib";
    public static final String VERSION = API.version_major+"."+API.version_minor+"."+API.version_patch;
    
    @SidedProxy(clientSide = "com.parkermc.mod.proxy.ProxyClient", serverSide = "com.parkermc.mod.proxy.ProxyServer", modId = MODID)
    public static ProxyCommon proxy;
    
    
    @EventHandler
    public void preInit(FMLPreInitializationEvent event){
    	proxy.preInit(event);
    }
    
    @EventHandler
    public void init(FMLInitializationEvent event) {
    	proxy.init(event);      
    }
    
    @EventHandler
    public void postInit(FMLPostInitializationEvent event) {
    	proxy.postInit(event);
    }
}

